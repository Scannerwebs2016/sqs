package com.example;

/**
 * Interface to model a queue
 * @author Jorge Marin Guerrero
 *
 * @param <E>
 */
public interface QueueService<E> {

  //
  // Task 1: Define me.
  //
  // This interface should include the following methods.  You should choose appropriate
  // signatures for these methods that prioritise simplicity of implementation for the range of
  // intended implementations (in-memory, file, and SQS).  You may include additional methods if
  // you choose.
  //
  // - push (offer)
  //   pushes a message onto a queue.
  // - pull (poll)
  //   retrieves a single message from a queue.
  // - delete
  //   deletes a message from the queue that was received by pull().
  //

	
	/**
	 * Inserts the message passed as parameter into TAIL of the queue
	 * @param e
	 * @return true in case it was inserted into the queue; otherwise, false
	 */
	boolean push(E message);
	
	/**
	 * Retrieves the closest to the HEAD visible message without removing it from the queue, setting the message as not visible
	 * @return visible message or null if there was not any visible message in the queue
	 */
	E pull();
	
	/**
	 * Removes the closest to the HEAD not visible message.
	 * 
	 * @param message to be removed from the queue. It must be not visible to be removed
	 * @return true in case the message was removed; otherwise, false
	 */
	boolean remove(E message);
	
	/**
	 * Returns the number of (visible and hidden) messages in the queue. 
	 * @return
	 */
	int size();
	
	/**
	 * Destroy the queue to free resources
	 * @return
	 */
	boolean destroy();
	
	/**
	 * Returns some statistics: incoming messages(total pushed messages), outgoing (total removed messages), 
	 * timeouts (total messages which status was changed from hidden to visible after reaching visibility timeout)
	 * @return
	 */
	String statistic();
	
	/**
	 * Updates the visibility time out queue property for one message or for the entire queue
	 * 
	 * @param message to update its visibilityTimeOut or null if the visibilityTimeOut has to be changed for the entire
	 * queue from that moment on)
	 * @param visibilityTimeOut in milliseconds
	 * @return
	 */
	void changeVisibilityTimeOut(E message, Long visibilityTimeOut);
}
