package com.example;

import com.amazonaws.services.sqs.AmazonSQSClient;
import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.services.sqs.model.ReceiveMessageRequest;
import com.amazonaws.services.sqs.model.ReceiveMessageResult;
import com.amazonaws.services.sqs.model.SendMessageRequest;

/**
 * I am not familiar with Amazon SQS, but it looks like the AmazonSQSClient always requires the queueName to 
 * operate the queue. I am changing the constructor here to pass the queueName as parameter because I have not found
 * a way to extract the queue URL from the AmazonSQSClient alone without a queueName.
 * 
 * @author Jorge Marin Guerrero
 *
 * @param <E>
 */
public class SqsQueueService implements QueueService<Message> {
	
	// Statistics
	private long incoming = 0;	// Messages successfully pushed
	private long outgoing = 0;	// Messages successfully removed
	private long timeouts = 0;	// Messages put back to the queue
	
	private final String queueUrl;
		
	
	private final AmazonSQSClient sqsClient;
	//
	// Task 4: Optionally implement parts of me.
	//
	// This file is a placeholder for an AWS-backed implementation of QueueService.
	// It is included
	// primarily so you can quickly assess your choices for method signatures in
	// QueueService in
	// terms of how well they map to the implementation intended for a production
	// environment.
	//
	public SqsQueueService(AmazonSQSClient sqsClient, final String queueName) {
		this.sqsClient = sqsClient;
		this.queueUrl = sqsClient.createQueue(queueName).getQueueUrl();
	}

	
	@Override
	public boolean destroy() {
		sqsClient.deleteQueue(queueUrl);
		return true;
	}
	
	@Override
	public boolean push(Message e) {
		SendMessageRequest sendMessageRequest = new SendMessageRequest(queueUrl, e.getBody());
		if( sqsClient.sendMessage(sendMessageRequest) != null ) {
			return true;
		}
		return false;
	}

	@Override
	public Message pull() {
		ReceiveMessageRequest receiveMessageRequest = new ReceiveMessageRequest(queueUrl);
		receiveMessageRequest.setMaxNumberOfMessages(1);
		ReceiveMessageResult receiveMessageResult = sqsClient.receiveMessage(receiveMessageRequest);
				
		if( !receiveMessageResult.getMessages().isEmpty() ) {
			incoming ++;
			return receiveMessageResult.getMessages().get(0);
		}else {
			return null;
		}		
	}

	@Override
	public boolean remove(Message e) {
		Message message = pull();
		if( message != null ) {
			sqsClient.deleteMessage(queueUrl, message.getReceiptHandle());
			outgoing ++;
	        return true;
		}else {
			return false;
		}		
	}

	@Override
	public int size() {
		// TODO I have not found a way to get the actual size of the queue from its client
		return -1;
	}
	
	@Override
	public String statistic() {
		StringBuilder sb = new StringBuilder("Incomings: ").append(incoming).append(" ,")
				.append("Outgoings:").append(outgoing).append(" ,")
				.append("Timmeouts: ").append(timeouts);
		return sb.toString();
	}

	@Override
	public void changeVisibilityTimeOut(Message e, Long visibilityTimeOut) {
		sqsClient.changeMessageVisibility(queueUrl, e.getReceiptHandle(), visibilityTimeOut.intValue());
	}


	
}
