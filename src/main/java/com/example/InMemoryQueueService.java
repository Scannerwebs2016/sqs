package com.example;

import com.amazonaws.util.StringUtils;

/**
 * Class to model a limited (max capacity=Integer.MAX_VALUE) and non blocking queue stored in memory.
 * 
 * @author Jorge Marin Guerrero
 *
 */
public final class InMemoryQueueService implements QueueService<CanvaMessage> {

	private CanvaMessage[] queue;
	private final String name;
	private final int CAPACITY;
	private final int HEAD;
	private final int TAIL = 0;
	private boolean stop;
	
	// Statistics
	private long incoming = 0; // Messages successfully pushed
	private long outgoing = 0; // Messages successfully removed
	private long timeouts = 0; // Messages put back to the queue after timeout reached

	/**
	 * Queue monitor thread responsible for monitoring the queue and make visible all those messages
	 * which reach the visibility time out.
	 */
	private final Thread monitor;
	
	public InMemoryQueueService(final String name, Integer capacity) {
		if ( StringUtils.isNullOrEmpty(name) || capacity == null || capacity < 1 ) {
			throw new IllegalArgumentException("At least one argument is not valid.");
		}

		this.name = name;
		this.CAPACITY = capacity;
		this.HEAD = CAPACITY - 1;
		this.queue = new CanvaMessage[CAPACITY];
		
		monitor = monitor();
		monitor.start();
	}

	private Thread monitor() {

		return new Thread(new Runnable() {
			@Override
			public void run() {
				while (!stop) {
					synchronized (queue) {
						// Wait until there is at least one NOT visible element in the queue
						while ( !stop && !isAnyHidden() ) {							
							try {
								queue.wait();
							} catch (InterruptedException e) {
								System.err.println("InterruptedException while monitoring the queue");
							}
						}

						// Extra protection against destroy
						if( stop ) { return; }
						

						for (int i = HEAD; i >= TAIL; i--) {

							// Review timer for all hidden messages in the queue
							if (queue[i] != null && !queue[i].isVisible()) {

								long waiting = System.currentTimeMillis() - queue[i].getPulledTime();
								if (waiting >= queue[i].getVisibilityTimeOut()) {
									
									queue[i].setVisible(true);

									if (i == HEAD) {
										// Already done
									} else {
										int auxiliarSize = 1;
										CanvaMessage[] aux = new CanvaMessage[CAPACITY];

										for (int j = HEAD; j >= TAIL; j--) {
											if ((i != j) && (queue[j] != null)) {
												aux[HEAD - auxiliarSize] = queue[j];
												auxiliarSize++;
											}
										}
										aux[HEAD] = queue[i];

										// Copy aux into queue
										for (int j = TAIL; j < CAPACITY; j++) {
											queue[j] = aux[j];
										}
										aux = null;
									}
									timeouts++;
								}
							}
						}
						queue.notifyAll();
					}
				}
			}
		}, "Monitor");
	}

	@Override
	public boolean destroy() {
		stop = true;
		
		synchronized (queue) {
			//monitor.interrupt();
			
			for (int i = TAIL; i <= HEAD; i++) {
				queue[i] = null;
			}
			queue = null;

			return true;
		}		
	}

	@Override
	public boolean push(CanvaMessage element) {
		if (element == null) {
			return false;
		}

		boolean added = false;
		try {
			synchronized (queue) {

				if( isAnyNull() ) {
					// Add element to the queue and release lock
					int insertPosition = HEAD - size();
					queue[insertPosition] = element;

					added = true;
					incoming++;
				}
				
				queue.notifyAll();
			}
		} catch (Exception e) {
			System.err.format("Exception launched while pushing element %s to the queue. Exception: %s \n", element,
					e.getMessage());
		}

		return added;
	}

	@Override
	public CanvaMessage pull() {
		CanvaMessage element = null;

		synchronized (queue) {
			if( isAnyVisible() ) {
				// Search for the closed visible message to the head
				CanvaMessage original = null;
				for (int i = HEAD; i >= TAIL; i--) {
					original = queue[i];
					if (original != null && original.isVisible()) {

						original.setVisible(false);
						original.setPulledTime(System.currentTimeMillis());

						element = original.clone(); // Element to be returned is cloned

						break;
					}
				}
			}
			queue.notifyAll();
		}
		return element;
	}

	public boolean remove(CanvaMessage element) {
		if (element == null) {
			return false;
		}
		boolean removed = false;

		synchronized (queue) {

			// Search for the message to be deleted, this message must be visible to be deleted
			for (int i = HEAD; i >= TAIL; i--) {
				if (queue[i] != null && queue[i].equals(element) && !queue[i].isVisible()) {

					queue[i] = null;

					int auxiliarSize = 0;
					CanvaMessage[] aux = new CanvaMessage[CAPACITY];

					// Create aux array removing any null between values and from HEAD to TAIL
					for (int j = HEAD; j >= TAIL; j--) {
						if (queue[j] != null) {
							aux[HEAD - auxiliarSize] = queue[j];
							auxiliarSize++;
						}
					}
					// Copy aux into queue
					for (int j = TAIL; j < CAPACITY; j++) {
						queue[j] = aux[j];
					}

					aux = null;
					removed = true;
					outgoing++;

					break;
				}
			}
		}
		return removed;
	}

	@Override
	public int size() {
		int size = 0;
		synchronized (queue) {
			for (CanvaMessage o : queue) {
				if (o != null) {
					size++;
				}
			}
		}
		return size;
	}

	@Override
	public String statistic() {
		StringBuilder sb = new StringBuilder("Incomings: ").append(incoming).append(" ,").append("Outgoings:")
				.append(outgoing).append(" ,").append("Timeouts: ").append(timeouts);
		return sb.toString();
	}

	private boolean isAnyVisible() {
		synchronized (queue) {
			for (CanvaMessage o : queue) {
				if (o != null && o.isVisible()) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isAnyNull() {
		synchronized (queue) {
			for (CanvaMessage o : queue) {
				if (o == null) {
					return true;
				}
			}
		}
		return false;
	}

	private boolean isAnyHidden() {
		synchronized (queue) {
			for (CanvaMessage o : queue) {
				if (o != null && !o.isVisible()) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public void changeVisibilityTimeOut(CanvaMessage message, Long visibilityTimeOut) {
		synchronized (queue) {
			if (size() != 0) {
				for (int i = TAIL; i <= HEAD; i++) {
					if( queue[i] != null ) {
						if (message == null) {
							queue[i].setVisibilityTimeOut(visibilityTimeOut);
						} else {
							if (queue[i].equals(message)) {								
								queue[i].setVisibilityTimeOut(visibilityTimeOut);								
								return;
							}
						}
					}					
				}
			}
		}
	}

	public String getName() {
		return this.name;
	}
}
