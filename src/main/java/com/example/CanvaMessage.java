package com.example;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;


import com.amazonaws.services.sqs.model.Message;
import com.amazonaws.util.Base64;
import com.amazonaws.util.StringUtils;

/**
 * Class to model a message to be used in FileQueueService and InMemoryQueueService.
 * This class is a wrapper for com.amazonaws.services.sqs.model.Message
 * 
 * @author Jorge Marin Guerrero
 *
 */
public class CanvaMessage implements Serializable, Cloneable {
	
	private static final long serialVersionUID = 3727571288781182190L;
	private static final String SEPARATOR = ";";
	
	private final String id;
	private final Message awsMessage;
	
	private boolean visible = true;
	private long pulledTime;		// Time when message was pulled successfully
	private long visibilityTimeOut;
	
	
	public CanvaMessage(Message awsMessage, Long visibilityTimeOut) {
		if( awsMessage == null || StringUtils.isNullOrEmpty(awsMessage.getMessageId()) || 
				visibilityTimeOut==null || visibilityTimeOut==-1) {
			throw new IllegalArgumentException("At least one argument is not valid.");
		}
		
		this.awsMessage = awsMessage;	
		this.id = awsMessage.getMessageId();
		this.visibilityTimeOut = visibilityTimeOut;
	}
	
	/**
	 * Private constructor only used by clone().
	 * @param id	 
	 * @param visible
	 * @param pulledTime
	 * @param visibilityTimeOut
	 * @param awsMessage
	 */
	private CanvaMessage(String id, boolean visible, long pulledTime, long visibilityTimeOut, Message awsMessage) {
		this.id = id;
		this.visible = visible;
		this.pulledTime = pulledTime;
		this.visibilityTimeOut = visibilityTimeOut;
		this.awsMessage = awsMessage;	
	}	
	
	public long getVisibilityTimeOut() {
		return visibilityTimeOut;
	}

	public void setVisibilityTimeOut(long visibilityTimeOut) {
		this.visibilityTimeOut = visibilityTimeOut;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}

	public long getPulledTime() {
		return pulledTime;
	}

	public void setPulledTime(long pulledTime) {
		this.pulledTime = pulledTime;
	}

	public String getId() {
		return id;
	}

	public Message getAwsMessage() {
		return awsMessage;
	}
	

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CanvaMessage other = (CanvaMessage) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	@Override
	public CanvaMessage clone() {
		return new CanvaMessage(id, visible, pulledTime, visibilityTimeOut, awsMessage.clone());
	}

	/**
	 * Parses a CanvaMessage string representation into a CanvaMessage object	
	 * @param message
	 * @return
	 */
	public static CanvaMessage parse(String message) {
		try ( ObjectInputStream ois = new ObjectInputStream(new ByteArrayInputStream(Base64.decode(message)))){
			return (CanvaMessage)ois.readObject();
		} catch (IOException | ClassNotFoundException e) {
			System.err.format("Exception parsing message %s.\n", message);
			return null;
		}    
	}
	
	/**
	 * Serializes the calling object into a Base64 string
	 * @return
	 */
	public String serialize() {
		try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
    			ObjectOutputStream oos = new ObjectOutputStream(baos)){
    		oos.writeObject(this);
    		
    		return Base64.encodeAsString(baos.toByteArray()); 
    	} catch (IOException e) {
			System.err.format("Exception serializing message %s.\n", this.id);
			return null;
		}
    } 
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("message=").append(awsMessage.toString()).append(SEPARATOR)
				.append("visible=").append(visible).append(SEPARATOR)
				.append("pulledTime=").append(pulledTime).append(SEPARATOR)
				.append("visibilityTimeOut=").append(visibilityTimeOut);
		
		return sb.toString();
	}
	
}
