package com.example;

import java.io.InputStream;
import java.util.Properties;

/**
 * Utility class to load properties based on the environment selected.
 * 
 * In order to select an environment:
 * mvn test -Denv=stage
 * 
 * If System property 'env' not found, then local.properties will be used
 * 
 * @author Jorge Marin Guerrero
 *
 */
public class PropertiesUtils {

	// Available environments with properties files 
	private static final String[] envNames = {"local", "stage", "production"};
	
	/**
	 * Retrieves the property value associated to the passed key from the corresponding properties file.
	 * The properties file is chosen based on the value of the System property 'env' (environment)
	 * 
	 * @param key
	 * @return
	 */
	protected static String getProperty(String key) {
		if( key == null ) {
			return null;
		}
		
		String propertiesFile = null;
		try {
			Properties properties = new Properties();
			String env = System.getProperty("env");
			//propertiesFile = (env != null ? env : "local") + ".properties"; // default properties file is local.properties
			
			for(String allowed : envNames) {
				if(allowed.equals(env)) {
					propertiesFile = env + ".properties";
					break;
				}
			}
			
			propertiesFile = (propertiesFile != null ? propertiesFile : "local.properties"); // default properties file is local.properties
						
			System.out.format("Loading [%s] file \n", propertiesFile);			
			
			InputStream stream = Thread.currentThread().getContextClassLoader().getResourceAsStream(propertiesFile);
			properties.load(stream);

			return properties.getProperty(key);
			
		}catch (Exception e) {
			System.err.format("Exception reading properties file [%s] ,property=%s . Exception: %s\n", propertiesFile, key, e.getMessage());
		}
		return null;
		
	}
}
