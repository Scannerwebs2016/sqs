package com.example;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.channels.FileChannel;
import java.nio.channels.FileLock;
import java.util.ArrayList;
import java.util.List;

import com.amazonaws.util.StringUtils;

/**
 * Class to model a limited (max capacity=Integer.MAX_VALUE) and non blocking queue stored in the file system
 * 
 * @author Jorge Marin Guerrero
 *
 */
public class FileQueueService implements QueueService<CanvaMessage> {

	private final File queue;
	private final String location;
	private final int CAPACITY;
	private FileLock lock;
	private boolean stop;
	
	private int countVisible = 0;	// Tracks the number of visible messages
	private int countHidden = 0;	// Tracks the number of NOT visible messages
	
	// Statistics
	private long incoming = 0;	// Messages successfully pushed
	private long outgoing = 0;	// Messages successfully removed
	private long timeouts = 0;	// Messages put back to the queue after timeout reached
	
	private final Thread monitor;
	
	public FileQueueService(final String location, final Integer capacity) throws IOException {
		if( StringUtils.isNullOrEmpty(location) || 
				capacity==null || capacity < 1) {
			throw new IllegalArgumentException("At least one argument is not valid.");
		}
		
		CAPACITY = capacity;
		this.location = location;
		queue = new File(location);
		if (!queue.createNewFile()) {
			throw new IOException("Impossible to create queue. File creation failed. Location: "+location);
		}
		
		monitor = monitor();
		monitor.start();
	}
	
	/**
	 * Queue monitor thread responsible for monitoring the queue and make visible all those messages
	 * which reach the visibility time out.
	 */
	private Thread monitor() {
		
		return new Thread(new Runnable() {	
			@Override
			public void run() {
				while( !stop ) {
					synchronized (queue) {
						//  Wait until there is at least one NOT visible element in the queue 
						while ( !stop && !isAnyHidden() ) {
							
							try {
								queue.wait();
							} catch (InterruptedException e) {
								System.err.println("InterruptedException while monitoring the queue");
							}
						}
						
						// Extra protection against destroy
						if( stop ) {
							return;
						}
						
						List<CanvaMessage> messages = read();	
						
						int updatesCounter = 0;
						List<CanvaMessage> updatedMessages = new ArrayList<>();
												
						for(int i = 0; i < messages.size() ; i++) {
							// Review timer for all hidden messages in the queue
							CanvaMessage message = messages.get(i);
							if( !message.isVisible()) {								
								long waiting = System.currentTimeMillis() - message.getPulledTime();
								if( waiting >= message.getVisibilityTimeOut() ) {
									
									CanvaMessage clone = message.clone();
									clone.setVisible(true);	
									updatedMessages.add(clone);	
									
									updatesCounter ++;
								}
							}			
						}
						
						if( !updatedMessages.isEmpty() ) {
							for(CanvaMessage message : messages) {
								if( !updatedMessages.contains(message) ) {
									updatedMessages.add(message);
								}
							}
							
							if( copy(updatedMessages) ) {
								timeouts = timeouts + updatesCounter;
								countHidden = countHidden- updatesCounter;
								countVisible = countVisible + updatesCounter;
							}						
						}						
						
						updatedMessages.clear();
						messages.clear();
						
						queue.notifyAll();
					}
				}
			}
		}, "Monitor");
		
	}
	
	@Override
	public boolean destroy() {
		stop = true;
		countHidden = 0;
		synchronized (queue) {
			//monitor.interrupt();
			
			// Remove queue file
			if( queue.exists() && queue.delete() ) {
				return true;
			}				
			
			return false;
		}
		
	}
	
	@Override
	public boolean push(CanvaMessage element) {
		if( element == null ) {
			return false;
		}
		boolean added = false;
		try {
			synchronized (queue) {
				// Wait while queue file is locked
				while ( isLocked()) {
					try {
						queue.wait();
					} catch (InterruptedException e1) {
						System.err.println("InterruptedException while waiting for pushing element into the queue");
					}
				}

				if( size() < CAPACITY) {
					// Add element to the queue and release lock
					added = write(element);
					if (added) {
						countVisible ++;
						incoming ++;
					}
				}				

				queue.notifyAll();
			}
		} catch (Exception e) {
			System.err.format("Exception launched while pushing element %s to the queue. Exception: %s \n", element, e.getMessage());
		}
		return added;
	}

	@Override
	public CanvaMessage pull() {
		CanvaMessage message = null;
		
		synchronized (queue) {

			// Wait while queue file is locked
			while ( isLocked()) {
				try {
					queue.wait();
				} catch (InterruptedException e) {
					System.err.println("InterruptedException while waiting for pulling an element from the queue");
				}
			}

			List<CanvaMessage> messages = read();
			
			if ( !messages.isEmpty() ) {				
				// Search for the first visible message close to the HEAD
				for(int i=0; i < messages.size(); i++) {
					CanvaMessage msg = messages.get(i);
					if( msg.isVisible() ) {
						// Only queue head element updated
						message = msg;
						message.setPulledTime(System.currentTimeMillis());
						message.setVisible(false);		
						
						// Copy the messages back to the queue with the message in the HEAD modified			
						if ( copy(messages) ) {							
							countHidden ++;
							countVisible --;
						}else {
							message = null;	
						}						
						break;
					}
				}				
			}			
			queue.notifyAll();
		}
		return message;		
	}

	@Override
	public boolean remove(CanvaMessage message) {
		if( message == null ) {
			return false;
		}
		boolean remove = false;
		
		synchronized (queue) {

			// Wait while queue file is locked
			while ( isLocked()) {
				try {
					queue.wait();
				} catch (InterruptedException e) {
					System.err.println("InterruptedException while waiting for pulling an element from the queue");
				}
			}
						
			List<CanvaMessage> messages = read();
			
			if ( !messages.isEmpty() ) {
								
				// Find message in messages to be removed	
				List<CanvaMessage> aux = new ArrayList<>();
				boolean found = false;
				for(CanvaMessage msg : messages) {
					if( !msg.equals(message)) {
						aux.add(msg);						
					}else {
						if( !msg.isVisible() ) {
							found = true;
						}
					}
				}
				if( found ) {
					messages.clear();
					messages.addAll(aux);
					aux.clear();
					
					// Copy the rest of messages back to the queue				
					if ( copy(messages) ) {
						countHidden --;
						outgoing ++;						
						
						remove = true; 
					}
				}			
			}	
		}
		return remove;		
	}
	
	@Override
	public int size() {
		synchronized (queue) {
			return countVisible + countHidden;
		}
	}	
	
	@Override
	public String statistic() {
		StringBuilder sb = new StringBuilder("Incomings: ").append(incoming).append(" ,")
				.append("Outgoings:").append(outgoing).append(" ,")
				.append("Timeouts: ").append(timeouts);
		return sb.toString();
	}
	
		
	private boolean isLocked() {
		if (lock != null && lock.isValid()) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Writes the message into the file queue.
	 * First element in the file corresponds to the queue HEAD message
	 * 
	 * @return true if the message was inserted correctly; otherwise, false.
	 */
	private boolean write(CanvaMessage message) {
		try ( FileOutputStream out = new FileOutputStream(queue, true)){
			lock = out.getChannel().tryLock();
			
			if( isLocked() ) {
				try(BufferedWriter bw =new BufferedWriter(new OutputStreamWriter(out))){
					bw.write(message.serialize());
					bw.newLine();
					return true;
				}				
			}			

		} catch (IOException e) {
			System.err.format("IOException launched while writting message '%s' into the queue. Exception: %s \n",
					message, e.getMessage());
		} finally {
			releaseLock();
		}
		return false;
	}

	/**
	 * Reads the queue file returning a list of the messages in the list.
	 * First element in the file corresponds to the queue HEAD message
	 * 
	 * @return a list with messages or empty.
	 */
	private List<CanvaMessage> read() {
		List<CanvaMessage> messages = new ArrayList<>();
		
		try (FileInputStream in = new FileInputStream(queue)){
			lock = in.getChannel().tryLock(0L, Long.MAX_VALUE, true);
			
			if( isLocked() ) {
				try(BufferedReader br = new BufferedReader(new InputStreamReader(in))){
					
					String message = "";
					int index = 0;
					while ((message = br.readLine()) != null) {
						messages.add(index++, CanvaMessage.parse(message));
					}					
				}
				return messages;
			}
			
		}catch (IOException e) {
			System.err.format("IOException launched while reading message from the queue. Exception: %s \n",e.getMessage());
		} finally {
			releaseLock();
		}	
		return messages;
	}
	
	private void releaseLock() {
		try {
			if (lock != null && lock.isValid() ) {
				lock.release();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * Insert all messages from the list into the queue file
	 * @param messages
	 * @return
	 */
	private boolean copy(List<CanvaMessage> messages) {
		File temp = null;
				
		try {
			temp = File.createTempFile("tmp-", ".tmp", new File(location).getParentFile());
			
			try(BufferedWriter bw = new BufferedWriter(new FileWriter(temp, true))){
				
				for(int i=0; i < messages.size(); i++) {
					bw.write(messages.get(i).serialize());
					bw.newLine();
					bw.flush();	
				}
				
				// Copy temp file into queue and remove
				try(FileInputStream src = new FileInputStream(temp);
					FileOutputStream dest = new FileOutputStream(queue)){
					
					FileChannel srcCh = src.getChannel();
					FileChannel destCh = dest.getChannel();
					
					lock = destCh.tryLock();
					if( isLocked() ) {
						destCh.transferFrom(srcCh, 0, srcCh.size());
						
						return true;	
					}					
				}							
			}				

		} catch (IOException e) {
			System.err.format("IOException launched while copying queue. Exception: %s \n", e.getMessage());
			e.printStackTrace();
		} finally {
			messages.clear();
			releaseLock();
			
			if( temp != null && temp.exists() ) {
				temp.delete();
			}
		}
		return false;
	}

	private boolean isAnyHidden() {
		return countHidden != 0;
	}

	@Override
	public void changeVisibilityTimeOut(CanvaMessage message, Long visibilityTimeOut) {
		List<CanvaMessage> messages = new ArrayList<>();
		
		synchronized (queue) {
			// Wait until there is not locks on the file
			while ( isLocked()) {
				try {
					queue.wait();
				} catch (InterruptedException e) {
					System.err.println("InterruptedException while waiting for changing timeout visibility");
				}
			}
			
			// Read messages from the file
			messages = read();
			
			// Change time out for only 1 messages
			for(CanvaMessage msg : messages) {
				if( message == null ) {
					msg.setVisibilityTimeOut(visibilityTimeOut);
				}else {
					if( msg.equals(message)) {
						msg.setVisibilityTimeOut(visibilityTimeOut);
						break;
					}
				}				
			}	
			
			copy(messages);
			
			queue.notifyAll();
		}
	}

	public String getLocation() {
		return this.location;
	}

	
}
