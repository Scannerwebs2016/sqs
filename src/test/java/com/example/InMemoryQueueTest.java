package com.example;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.amazonaws.services.sqs.model.Message;

public class InMemoryQueueTest {	
	
	protected QueueService<CanvaMessage> queue;
	protected static int capacity;
	private static String name;
	
	// Timeout is big enough to avoid any message timeout => push and pull order will be the same;
	protected static final long visibilityTimeout = 10000;  // 10 seconds
	
	protected static long numberOfMgsToProduce1;
	protected static long numberOfMgsToProduce2;
	
	protected static Message awsMessage1;
	protected static Message awsMessage2;
	protected static CanvaMessage message1;
	protected static CanvaMessage message2;

	@BeforeClass
	public static void init() {
		capacity = Integer.parseInt(PropertiesUtils.getProperty("memory.queue.capacity"));
		name = PropertiesUtils.getProperty("memory.queue.name");
		System.out.format("InMemoryQueue configuration: [Capacity: %d] \n", capacity);
			
		numberOfMgsToProduce1 = 30;
		numberOfMgsToProduce2 = 15;
	}
	
	@Before
	public void setUp() {	
		awsMessage1 = new Message();
		awsMessage1.setMessageId(UUID.randomUUID().toString());
		awsMessage1.setBody("Message 1");
		message1 = new CanvaMessage(awsMessage1, visibilityTimeout);
		
		awsMessage2 = new Message();
		awsMessage2.setMessageId(UUID.randomUUID().toString());
		awsMessage2.setBody("Message 2");
		message2 = new CanvaMessage(awsMessage2, visibilityTimeout);
		
		queue = new InMemoryQueueService(name, capacity);
	}
	
	@After
	public void tearDown() {
		assertTrue(queue.destroy());
	}
	
	
	@Test
	public void testConstructor_fails() {
		String expectedErrorMsg = "At least one argument is not valid.";
		// Name is null
		try {
			new InMemoryQueueService(null, 1);
		}catch (IllegalArgumentException e) {
			assertEquals(expectedErrorMsg, e.getMessage());
		}
		// Name is empty
		try {
			new InMemoryQueueService("", 1);
		}catch (IllegalArgumentException e) {
			assertEquals(expectedErrorMsg, e.getMessage());
		}	
		
		// Capacity is null
		try {
			new InMemoryQueueService(name, null);
		}catch (IllegalArgumentException e) {
			assertEquals(expectedErrorMsg, e.getMessage());
		}
		// Capacity < 1
		try {
			new InMemoryQueueService(name, 0);
		}catch (IllegalArgumentException e) {
			assertEquals(expectedErrorMsg, e.getMessage());
		}	
	}

	@Test
	public void testPush_overflowProtection() {		
		for( int i = 0; i < capacity; i++) {
			Message awsMessage = new Message();
			String id = UUID.randomUUID().toString();
			awsMessage.setMessageId(id);
			awsMessage.setBody("Message "+ id);
			
			assertTrue(queue.push(new CanvaMessage(awsMessage, visibilityTimeout)));
		}
		assertEquals(capacity, queue.size());
		
		// Trying to push a new message does not increase the size
		// because the message has not been inserted
		assertFalse(queue.push(message1));
		assertEquals(capacity, queue.size());
	}
	
	@Test
	public void testPush_ok() {
		assertTrue(queue.push(message1));
		assertEquals(1, queue.size());
	}

	@Test
	public void testPush_nullNotInserted() {
		assertFalse(queue.push(null));
		assertEquals(0, queue.size());
	}

	@Test
	public void testPull_withEmptyQueue() {
		assertNull(queue.pull());		
	}
	
	@Test
	public void testPull_doesNotRemove() {
		assertTrue(queue.push(message1));
		assertEquals(1, queue.size());
		assertNotNull(queue.pull());	
		assertEquals(1, queue.size());
	}
	
	@Test
	public void testPull_keepInsertionOrder() {
		assertTrue(queue.push(message1));
		assertTrue(queue.push(message2));
		
		assertEquals("Message 1", queue.pull().getAwsMessage().getBody());	
		assertEquals("Message 2", queue.pull().getAwsMessage().getBody());		
	}
	
	@Test
	public void testRemove_nullMessage() {
		assertFalse(queue.remove(null));
	}
	
	@Test
	public void testRemove_ok() {
		assertTrue(queue.push(message1));
		assertTrue(queue.push(message2));
		assertEquals("Message 1", queue.pull().getAwsMessage().getBody());	
		
		assertTrue(queue.remove(message1));
		assertFalse(queue.remove(message2));
		
		assertEquals(1, queue.size());
	}
	
	@Test
	public void testStatistic() {
		assertTrue(queue.statistic().contains("Incomings: 0 ,Outgoings:0 ,Timeouts: 0"));
		queue.push(message1);
		queue.pull();
		queue.remove(message1);
		assertTrue(queue.statistic().contains("Incomings: 1 ,Outgoings:1 ,Timeouts: 0"));
	}
	
	@Test
	public void testChangeVisibilityTimeOut_forAllMessages_emptyQueue() {
		queue.changeVisibilityTimeOut(null, 0L);
	}
	
	@Test
	public void testChangeVisibilityTimeOut_forSelectedMessage_emptyQueue() {
		queue.changeVisibilityTimeOut(message1, 0L);
	}
	
	/**
	 * Timeout 1 second set to fail the test in case the wait is taken so long 
	 */
	@Test(timeout=1000)
	public void testChangeVisibilityTimeOut_forAllMessages() {
		
		// Insert into the queue
		queue.push(message1);
		queue.push(message2);
		
		// Pulling them will make search-able by the queue monitor
		assertEquals(message1, queue.pull());
		assertEquals(message2, queue.pull());
		assertNull(queue.pull());	// No messages visible now
		
		// Setting timeout 0 seconds for ALL messages
		queue.changeVisibilityTimeOut(null, 0L);
		
		// Wait till the queue monitor change the messages to visible again
		// this time is unKnown, but it is smaller than visibilityTimeout
		long start = System.currentTimeMillis();
				
		// Pull and remove in one go in order not to interfere with the other message 
		while( !queue.remove(queue.pull()) ) {
			// Wait till monitor puts at least one message back to visible and 
			// then it can be pulled and removed
		}
		long wait = System.currentTimeMillis()-start;
		assertTrue(visibilityTimeout > wait);
			
		// the other message should remain NOT visible because its timeout is visibilityTimeout = 10 seconds
		assertNotNull(queue.pull());	
	}
	
	@Test(timeout=1000)
	public void testChangeVisibilityTimeOut_differentTimeOuts() {
		
		// Insert into the queue
		queue.push(message1);
		queue.push(message2);
		
		// Pulling them will make search-able by the queue monitor
		assertEquals(message1, queue.pull());
		assertEquals(message2, queue.pull());
		assertNull(queue.pull());	// No messages visible now
		
		// Setting timeout 300 milliseconds for message1
		queue.changeVisibilityTimeOut(message1, 300L);
		
		// Wait till the queue monitor change the messages to visible again
		// this time is unKnown, but it is smaller than visibilityTimeout
		long start = System.currentTimeMillis();
		
		// Pull and remove in one go in order not to interfere with the other message 
		while( !queue.remove(queue.pull())) {
			// Wait till monitor puts message1 back to visible and then it
			// can be pulled and removed
		}
		long wait = System.currentTimeMillis()-start;
		assertTrue(visibilityTimeout > wait);
						
		// message2 should remain NOT visible because its timeout is visibilityTimeout is still 10 seconds
		assertNull(queue.pull());	
	}
	
}
