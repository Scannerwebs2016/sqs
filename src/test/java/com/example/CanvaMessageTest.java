package com.example;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import com.amazonaws.services.sqs.model.Message;

public class CanvaMessageTest {
	
	private final String encodedMsg = "rO0ABXNyABhjb20uZXhhbXBsZS5DYW52YU1lc3NhZ2Uzuv5Wbr9U7gIABUoACnB1bGxlZFRpbWVKABF2aXNpYmlsaXR5VGltZU91dFoAB3Zpc2libGVMAAphd3NNZXNzYWdldAAqTGNvbS9hbWF6b25hd3Mvc2VydmljZXMvc3FzL21vZGVsL01lc3NhZ2U7TAACaWR0ABJMamF2YS9sYW5nL1N0cmluZzt4cAAAAAAAAAAAAAAAAAAAAAEBc3IAKGNvbS5hbWF6b25hd3Muc2VydmljZXMuc3FzLm1vZGVsLk1lc3NhZ2U++SiSOdeoHgIAB0wACmF0dHJpYnV0ZXN0ACdMY29tL2FtYXpvbmF3cy9pbnRlcm5hbC9TZGtJbnRlcm5hbE1hcDtMAARib2R5cQB+AAJMAAltRDVPZkJvZHlxAH4AAkwAFm1ENU9mTWVzc2FnZUF0dHJpYnV0ZXNxAH4AAkwAEW1lc3NhZ2VBdHRyaWJ1dGVzcQB+AAVMAAltZXNzYWdlSWRxAH4AAkwADXJlY2VpcHRIYW5kbGVxAH4AAnhwcHQAHlRoaXMgaXMgdGhlIGJvZHkgb2YgbXkgbWVzc2FnZXBwcHQAATFwcQB+AAg=";
	private Message awsMessage;
	private CanvaMessage message;
	
	@Test
	public void test_constructor() {
		String expectedErrorMsg = "At least one argument is not valid.";
		Message awsMessage = new Message();
		// Message is null
		try {
			new CanvaMessage(null, 1L);
		}catch (IllegalArgumentException e) {
			assertEquals(expectedErrorMsg, e.getMessage());
		}
		
		// Message not null, but messageId empty or null
		try {
			new CanvaMessage(awsMessage, 1L);
		}catch (IllegalArgumentException e) {
			assertEquals(expectedErrorMsg, e.getMessage());
		}
		
		awsMessage.setMessageId("1");
				
		// visibilityTimeOut is null
		try {
			new CanvaMessage(awsMessage, null);
		}catch (IllegalArgumentException e) {
			assertEquals(expectedErrorMsg, e.getMessage());
		}
		
		// visibilityTimeOut < 0
		try {
			new CanvaMessage(awsMessage, null);
		}catch (IllegalArgumentException e) {
			assertEquals(expectedErrorMsg, e.getMessage());
		}
		
	}
	
	@Test
	public void test_serialize() {
		awsMessage = new Message();
		awsMessage.setMessageId("1");
		awsMessage.setBody("This is the body of my message");
		message = new CanvaMessage(awsMessage, 1L);
		
		assertEquals(encodedMsg, message.serialize());
	}
	
	@Test
	public void test_parse() {
		message = CanvaMessage.parse(encodedMsg);
		awsMessage = message.getAwsMessage();
		
		assertEquals("1", awsMessage.getMessageId());
		assertEquals("This is the body of my message", awsMessage.getBody());		
	}
	
	@Test
	public void test_clone() {
		awsMessage = new Message();
		awsMessage.setMessageId("1");
		awsMessage.setBody("This is the body of my original message");
		message = new CanvaMessage(awsMessage, 1L);
		message.setPulledTime(1);
		message.setVisible(true);
		message.setVisibilityTimeOut(1);
		
		CanvaMessage clone = message.clone();
		assertEquals(message.getId(), clone.getId());
		assertEquals(message.getPulledTime(), clone.getPulledTime());
		assertEquals(message.getVisibilityTimeOut(), clone.getVisibilityTimeOut());
		assertEquals(message.isVisible(), clone.isVisible());
		assertEquals(message.getAwsMessage(), clone.getAwsMessage());
		
		
		// Changing attributes in cloned message should not affect to the original one
		clone.setPulledTime(2);
		clone.setVisible(false);
		clone.setVisibilityTimeOut(2);
		clone.getAwsMessage().setMessageId("2");
		clone.getAwsMessage().setBody("This message was modified");
		
		// Original message will keep having the original values
		assertEquals("1", message.getId());
		assertEquals(1, message.getPulledTime());
		assertEquals(1, message.getVisibilityTimeOut());
		assertEquals(true, message.isVisible());
		assertEquals("This is the body of my original message", message.getAwsMessage().getBody());
	}
}
