package com.example;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.amazonaws.services.sqs.model.Message;

public class FileQueueTest extends InMemoryQueueTest {

	private static String path;
		
	@BeforeClass
	public static void init() {
		capacity = Integer.parseInt(PropertiesUtils.getProperty("file.queue.capacity"));
		String queue_path = PropertiesUtils.getProperty("file.queue.path");
		try {	
			path = new StringBuilder(new File(".").getCanonicalPath()).append(File.separator)
					.append("target").append(File.separator)
					.append(queue_path).toString();
		} catch (IOException e) {
			assert false;
		}
		System.out.format("FileQueue configuration: [Capacity: %d, Location: %s]\n", capacity, path);
		
		numberOfMgsToProduce1 = 12;
		numberOfMgsToProduce2 = 15;
	}
    
	@Before
	public void setUp() {
		awsMessage1 = new Message();
		awsMessage1.setMessageId(UUID.randomUUID().toString());
		awsMessage1.setBody("Message 1");
		message1 = new CanvaMessage(awsMessage1, visibilityTimeout);
		
		awsMessage2 = new Message();
		awsMessage2.setMessageId(UUID.randomUUID().toString());
		awsMessage2.setBody("Message 2");
		message2 = new CanvaMessage(awsMessage2, visibilityTimeout);
		
		try {	
			queue = new FileQueueService(path, capacity);				
		} catch (IOException e) {
			e.printStackTrace();
			assert false;
		}
	}
	
	@After
	public void tearDown() {
		assertTrue(queue.destroy());
	}
	
	@Test
	public void testConstructor_fails() {
		String expectedErrorMsg = "At least one argument is not valid.";
		// Path is null
		try {
			new FileQueueService(null, capacity);
		}catch (IllegalArgumentException | IOException e) {
			assertEquals(expectedErrorMsg, e.getMessage());
		}
		// Path is empty
		try {
			new FileQueueService("", capacity);
		}catch (IllegalArgumentException | IOException e) {
			assertEquals(expectedErrorMsg, e.getMessage());
		}		
		// Capacity is null
		try {
			new FileQueueService(path, null);
		}catch (IllegalArgumentException | IOException e) {
			assertEquals(expectedErrorMsg, e.getMessage());
		}
		// Capacity < 1
		try {
			new FileQueueService(path, 0);
		}catch (IllegalArgumentException | IOException e) {
			assertEquals(expectedErrorMsg, e.getMessage());
		}	
		
		// Queue file already exists
		try {
			new FileQueueService(path, 100);
		}catch (IllegalArgumentException | IOException e) {
			assertEquals("Impossible to create queue. File creation failed. Location: "+path, e.getMessage());
		}			
	}
	
}
