package com.example;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PropertiesUtilTest {

	private String environment;
	
	@Before
	public void setUp() {
		environment = System.getProperty("env");
		// Setting environment to local
		System.setProperty("env", "local");
	}
	
	@After
	public void tearDown() {
		// Reverting back environment change in case "env" was set initially
		if( environment != null ) {
			System.setProperty("env", environment);
		}		
	}
	
	
	@Test
	public void testGetProperty_nullProperty() {
		assertNull(PropertiesUtils.getProperty(null));
	}
	
	@Test
	public void testGetProperty_emptyProperty() {
		assertNull(PropertiesUtils.getProperty(""));
	}
	
	@Test
	public void testGetProperty_nonExistigProperty() {
		assertNull(PropertiesUtils.getProperty("nonExistigProperty"));
	}
	
	@Test
	public void testGetProperty_nonExistigPropertiesFile() {
		// Setting environment to nonExistingPropertyFile
		System.setProperty("env", "nonExistingPropertyFile");
		
		assertNull(PropertiesUtils.getProperty("whatEverProperty"));
	}
	
	@Test
	public void testGetProperty_existingProperty() {
		// memory.queue.capacity is 10 for environment local
		assertEquals("10", PropertiesUtils.getProperty("memory.queue.capacity"));
		
		// Setting environment to production
		System.setProperty("env", "production");
				
		// memory.queue.capacity is 1000 for environment prod
		assertEquals("1000", PropertiesUtils.getProperty("memory.queue.capacity"));
	}
	
	
}
